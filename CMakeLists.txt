cmake_minimum_required(VERSION 3.2 FATAL_ERROR)
project(Accordeur VERSION 0.1 LANGUAGES C CXX)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_subdirectory(portaudio EXCLUDE_FROM_ALL)
add_subdirectory(g3d2 EXCLUDE_FROM_ALL)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake/Modules/")

add_library( libleap SHARED IMPORTED )

if (WIN32)
	if("${CMAKE_GENERATOR}" MATCHES "(Win64|IA64|x64)") #MSVC x64
		set(MSVC_ARCH x64)
    else()
    	set(MSVC_ARCH x64)
	endif()

	set(LEAP_DLL_LOCATION ${CMAKE_SOURCE_DIR}/leap/lib/windows/${MSVC_ARCH}/Leap.dll)
	set(LEAP_IMPLIB_LOCATION ${CMAKE_SOURCE_DIR}/leap/lib/windows/${MSVC_ARCH}/Leap.lib)

	set_target_properties( libleap PROPERTIES IMPORTED_LOCATION ${LEAP_DLL_LOCATION} )
	set_target_properties( libleap PROPERTIES IMPORTED_IMPLIB ${LEAP_IMPLIB_LOCATION} )
else()
	set_target_properties( libleap PROPERTIES IMPORTED_LOCATION ${CMAKE_SOURCE_DIR}/leap/lib/linux/x64/libLeap.so )
endif()

add_executable(theremine "src/theremine.cpp")
target_link_libraries(theremine portaudio libleap g3d2)

add_executable(theremine_new "src/theremine_new.cpp")
target_link_libraries(theremine_new portaudio libleap g3d2)

if (WIN32)
	add_custom_command(TARGET theremine POST_BUILD    	# Adds a post-build event to MyTest
    	COMMAND ${CMAKE_COMMAND} -E copy_if_different  	# which executes "cmake - E copy_if_different..."
        ${LEAP_DLL_LOCATION}     						# <--this is in-file
        $<TARGET_FILE_DIR:theremine>)                 	# <--this is out-file path

    add_custom_command(TARGET theremine POST_BUILD    	# Adds a post-build event to MyTest
    	COMMAND ${CMAKE_COMMAND} -E copy_if_different  	# which executes "cmake - E copy_if_different..."
        $<TARGET_FILE:portaudio>    					# <--this is in-file
        $<TARGET_FILE_DIR:theremine>)                 	# <--this is out-file path
endif()

add_custom_command(TARGET theremine POST_BUILD    	# Adds a post-build event to MyTest
	COMMAND ${CMAKE_COMMAND} -E copy_directory  	# which executes "cmake - E copy_if_different..."
    ${CMAKE_SOURCE_DIR}/shaders   					# <--this is in-file
    $<TARGET_FILE_DIR:theremine>)                 	# <--this is out-file path

include_directories(portaudio/include ${CMAKE_SOURCE_DIR}/leap/include)