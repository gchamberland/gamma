# Gamma Project #

Gamma is synthetizer using a Leap Motion as controller and offering a few graphical animation to
offer a best interactivity feeling for the user.

## Installing Leap motion driver ##

Leap motion's driver is provided in the current distribution.
You have to install this dirver prior to use Gamma.
For information, Gamma rely on the old Leap driver (2.3.1).

On Windows 10, once installed, THIS DRIVER NEED A FIX TO WORK CORRECTLY :
	- Be sure Leap's driver is already installed !
	- Stop LeapService (Task Manager > Services)
	- Replace files "LeapSvc.exe" and "LeapSvc64.exe" in folder C:\Program Files (x86)\Leap Motion\Core Services
		by those provided in the current distribution.
	- Start LeapService again
	- Now when plugging in the Leap controller the icon in the task bar should be green, if yes,
		you are ready to use Gamma. Otherwise call Greg (gregoire.chamberland@gmail.com) !!

## Launching Gamma ##

Theremine_new.exe is the last version of Gamma and this one should be launched by a simple double click.

## Configuring Gamma ##

By default Gamma try to load a file called "config.json" in the same directory that the executable.
If this file exists, Gamma load it. If this file don't exists, run Gamma a first time, and press "S" : it should create the "config.json" file.
Currently, all parameters in config.json are used. These parameters can also be edited at runtime and then saved (Key "S").

Here are the controls allowing to edit parameters :
	- RIGHT and LEFT : moving the tape horizontally.
	- UP and DOWN : Increase the tape size.
	- O and L : Increase the number of light slots.
	- T and G : Rotate gamma display.

## Changing sound output device ##

Gamma use the system default device if nothing else is specified. But changing sound output device is possible in the configuration file
thank's to the parameter "soundDeviceId". 
You can list allowed value by running Gamma from a command line with the switch "-l" (example : theremine_new.exe -l).
You can also specify the output device thank's to the switch "-d" (example : theremine_new.exe -d 4).

## Other functions ##

Fullscreen mode can also be activation with the switch "-f" (exmaple : theremine_new.exe -f).
All switch can be combinated EXCEPT the "-l" one which will make Gamma print the list of available devices and then exit.

## Troubleshooting ##

Gamma don't start:
	- Make sure specified output device index is correct. Try to let Gamma select the default device by deleting the soundDeviceId parameter in the 
		configuration file and by not using the "-d" switch.
	- Start Gamma from a command line and see if a message is logged before the crash.
	- Verify the architecture of the system : the current release target Windows x64 so it can't work on any other arch or OS.
	- In last resort : call Greg (gregoire.chamberland@gmail.com)

My sound device is not listed :
	- Please verify it work with other app (vlc, browser, etc...)

I don't no how to use the "-l" switch :
	- You have to start Gamma from a command line and type "theremine_new.exe -l" (without the double quotes) and the press enter.

I don't know what is a command line :
	- Damn, let say it in french : "On est pas dans la merde..."
	- It mean "we are going see together how to do" :
		- Open the folder where Gamma is located
		- Edit the folder path on the top on the window to add "cmd cd " (with the la space, and without double quotes) BEFORE the path of the folder.
		- Now press "Enter" : welcome in your first command line !

Anything else :
	- Call Greg (gregoire.chamberland@gmail.com)