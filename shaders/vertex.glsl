#version 330

attribute vec2 pPosition;

varying vec2 vPosition;

void main() {
	vPosition = pPosition;
    gl_Position = vec4(vPosition, 0.0, 1.0);
}