#version 330

uniform mediump float fTime;

uniform mediump vec2 fCenter = vec2(0.0f);
uniform lowp int iSlotCount = 30;
uniform mediump float fSlotsOpacity = 0.8;
uniform mediump float fTapeSize = 0.5;
uniform float fTapeWidth = 0.0;
uniform float fTapeRotation = 0.0;

uniform mediump vec3 vLeftHand;
uniform mediump vec3 vRightHand;

varying mediump vec2 vPosition;

float rand(vec2 co){
	return fract(sin(dot(co.xy ,vec2(12.9898,78.233f))) * 43758.5453f);
}

vec4 renderSlot(float px, float sx, float h, float color, float redComp, float distortion) {
	float center1 = sx + (cos(vPosition.y * 50.0f - (fTime * 100.0f)) * (0.001f / (0.85f - distortion)));
	float center2 = sx - (sin(vPosition.y * 200.0f - (fTime * 31.0f)) * (0.002f / (0.85f - distortion)));
	
	vec4 condition = step(vec4(center1 - h, px, center2 - h, px), vec4(px, center1 + h, px, center2 + h));
	
	return vec4(color) * condition.x * condition.y +
		   vec4(color * redComp, 0.0f, color, 1.0f) * condition.z * condition.w;
}

vec4 slot(vec2 p, float sx, int sIndex, float distortion) {
	float color = cos(rand(vec2(p.x, p.y*fTime)) - sIndex* 2000+ p.y + fTime * 10.0) * 0.5 + 0.5;
	return renderSlot(p.x, sx, 0.001, color, float(iSlotCount - 3 - sIndex) / iSlotCount, distortion);
}

vec4 slots(vec2 p) {
	float start = fCenter.x - fTapeSize * 0.5;
	float offset = fTapeSize / float(iSlotCount - 1);
	vec4 result = vec4(0.0);
	float slotPosition = start;

	if (vLeftHand.y < 0.15 && vRightHand.y < 0.15) {
		for (int i=0; i<iSlotCount; i++) {
			slotPosition = start + offset * i;
			result += slot(p, slotPosition, i, - 0.15);
		}
	} else {
		for (int i=0; i<iSlotCount; i++) {
			if (abs(int(vLeftHand.x * iSlotCount) - i) < 3)  {
				slotPosition = start + offset * i;
				result += slot(p, slotPosition, i, vLeftHand.z) * (1.0 - 2 * (abs(vPosition.y + 0.4 - vLeftHand.y)));
			}

			if (abs(int(vRightHand.x * iSlotCount) - i) < 3) {
				slotPosition = start + offset * i;
				result += slot(p, slotPosition, i, vRightHand.z) * (1.0 - 2 * (abs(vPosition.y + 0.4 - vRightHand.y)));
			}
		}
	}
	return result;
}

vec4 tape(vec2 fragPos, float handPos, float modulation) {
	float halfSize = fTapeSize * 0.5;
	
	float osc = sin(fTime * int(100.0f * modulation)) * 0.4f + 0.6f;

	vec2 botLeft = step(vec2(fCenter.x - halfSize, fCenter.y + handPos - fTapeWidth + osc * 0.05f), fragPos);
	vec2 topRight = step(fragPos, vec2(fCenter.x + halfSize, fCenter.y + handPos + fTapeWidth + osc * 0.05f));

	return vec4(botLeft.x * botLeft.y * topRight.x * topRight.y);
}

void main() {
	mat2 rotMat = mat2(cos(fTapeRotation), -sin(fTapeRotation),
						sin(fTapeRotation), cos(fTapeRotation));
	
	vec2 finalPos = vPosition * rotMat;

	vec2 hand = vec2(vRightHand.y, vLeftHand.y) * 1.5f;

	vec4 color = tape(finalPos, hand.x, vRightHand.z) + tape(finalPos, hand.y, vLeftHand.z);

    gl_FragColor = color + slots(finalPos) * step(color.x, 0.0f) * fSlotsOpacity;
}