#include "abstract_synthetizer.h"
#include "delay.h"

#include <SFML/Window.hpp>

#include <iostream>

#define TWO_PI 6.28318530718f

class KeyboardSynth : public AbstractSynthetizer {
	private:
		float octave = 1;
		unsigned int sinIndex = 0;
		Delay d1 = Delay(0.17f, 44100, 0.3f);
		Delay d2 = Delay(0.3f, 44100, 0.5f);

		static const std::vector<float> &semiTonicRange() {
			static std::vector<float> semiTonicRange = {
				  32.70f,
				  34.65f,
				  36.71f,
				  38.89f,
				  41.20f,
				  43.65f,
				  46.25f,
				  49.00f,
				  51.91f,
				  55.00f,
				  58.27f,
				  61.74f,
				  65.41f
			};
			return semiTonicRange;
		}

		inline float genSinWave(float freq) {
		  return std::sin(sinIndex * TWO_PI * (freq / getSampleRate()));
		}

		inline float genSquareWave(float freq) {
			return genSinWave(freq) >= 0.0f ? 1.0f : 0.0f;
		}

		inline float genTriangleWave(float freq) {
			float p = 1.0f / (freq / getSampleRate());
			float localTime = std::fmodf((float) sinIndex, p);
			float val = localTime / p;

			return val < 0.25f ? val * 4.0f :
					val < 0.75f ? 2.0f - val * 4.0f :
					 val * 4.0f - 4.0f;
		}

		inline float genSawtoothWave(float freq) {
			float p = 1.0f / (freq / getSampleRate());
			float localTime = fmodf((float) sinIndex, p);

			return localTime / p * 2.0f - 1.0f;
		}

		float genTone(float freq) {
		  return (genSawtoothWave(freq) + genSawtoothWave(freq * 2.0f)
		  			+ genSawtoothWave(freq * 4.0f) + genSawtoothWave(freq * 8.0f)) * 0.25f;
		}

		inline float genToneIfPressed(const sf::Keyboard::Key key, int toneIndex) {
        	return sf::Keyboard::isKeyPressed(key) ?
        		genTone(semiTonicRange()[toneIndex] * octave) : 0;
		}

	protected:
			virtual int synthetizeSound(
			void *outputBuffer,
			unsigned long framesPerBuffer,
			const PaStreamCallbackTimeInfo* timeInfo,
			PaStreamCallbackFlags statusFlags
		) {
			float *out = reinterpret_cast<float *>(outputBuffer);
			
			//std::cout << octaveRight << std::endl;

			for (unsigned long i=0; i<framesPerBuffer; i++) {
				int nbPressed = 0;
				float finalSample = genToneIfPressed(sf::Keyboard::Q, 0);
				finalSample += genToneIfPressed(sf::Keyboard::Z, 1);
				finalSample += genToneIfPressed(sf::Keyboard::S, 2);
				finalSample += genToneIfPressed(sf::Keyboard::E, 3);
				finalSample += genToneIfPressed(sf::Keyboard::D, 4);
				finalSample += genToneIfPressed(sf::Keyboard::F, 5);
				finalSample += genToneIfPressed(sf::Keyboard::T, 6);
				finalSample += genToneIfPressed(sf::Keyboard::G, 7);
				finalSample += genToneIfPressed(sf::Keyboard::Y, 8);
				finalSample += genToneIfPressed(sf::Keyboard::H, 9);
				finalSample += genToneIfPressed(sf::Keyboard::U, 10);
				finalSample += genToneIfPressed(sf::Keyboard::J, 11);
				finalSample += genToneIfPressed(sf::Keyboard::K, 12);

				finalSample /= 13.0f;

				finalSample = d2.process(d1.process(finalSample));

				*out++ = finalSample;
				*out++ = finalSample;

				sinIndex++;
			}

			return 0;
		}

	public:

		
};

int main(void) {

	KeyboardSynth synth;
	synth.start();

	std::cin.get();

	return 0;
}