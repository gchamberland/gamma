#ifndef DELAY_H
#define DELAY_H

#include <stdexcept>

class Delay {
	private:
		float *buffer;
		float decay;
		size_t samples;
		
		size_t write;
		size_t read;

	public:
		Delay() :Delay(0, 0, 0) {

		}

		Delay(float delay, float sampleRate, float decay) {
			this->samples = (size_t) (sampleRate * delay);
			this->decay = decay;

			buffer = new float[samples];

			if (buffer == NULL) {
				std::cout << "Out of memory" << std::endl;
				throw new std::runtime_error("Out of memory !");
			}

			for (int i=0; i<samples; i++) {
				buffer[i] = 0;
			}

			write = samples - 1;
			read = 0;
		}

		~Delay() {
			delete buffer;
		}

		void setDecay(float decay) {
			this->decay = decay;
		}

		float process(float sample) {
			float current = buffer[read] * decay + sample;
			buffer[write] = current;
			write = read;
			if (read++ == samples) {
				read = 0;
			}
			return current;
		}
};

#endif