
#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif

#include <portaudio.h>

#include "Leap.h"

#include <iostream>
#include <cstring>
#include <mutex>

using namespace Leap;

class SampleListener : public Listener {
  public:
    virtual void onInit(const Controller&);
    virtual void onConnect(const Controller&);
    virtual void onDisconnect(const Controller&);
    virtual void onExit(const Controller&);
    virtual void onFrame(const Controller&);
    virtual void onFocusGained(const Controller&);
    virtual void onFocusLost(const Controller&);
    virtual void onDeviceChange(const Controller&);
    virtual void onServiceConnect(const Controller&);
    virtual void onServiceDisconnect(const Controller&);

  private:
};

std::mutex mutex;

const float tones[] = {
  32.70f,
  //34.65f,
  73.42f,
  //38.89f,
  41.20f,
  43.65f,
  //46.25f,
  49.00f,
  //51.91f,
  110.00f,
  //58.27f,
  61.74f
};

static float freqRight = 0;
static float volumeRight = 0.1;
static float modulationRight = 0;


static float freqLeft = 0;
static float volumeLeft = 0.1;
static float modulationLeft = 0;

void SampleListener::onInit(const Controller& controller) {
  std::cout << "Initialized" << std::endl;
}

void SampleListener::onConnect(const Controller& controller) {
  std::cout << "Connected" << std::endl;
  //controller.enableGesture(Gesture::TYPE_CIRCLE);
  //controller.enableGesture(Gesture::TYPE_KEY_TAP);
  //controller.enableGesture(Gesture::TYPE_SCREEN_TAP);
  //controller.enableGesture(Gesture::TYPE_SWIPE);
}

void SampleListener::onDisconnect(const Controller& controller) {
  // Note: not dispatched when running in a debugger.
  std::cout << "Disconnected" << std::endl;
}

void SampleListener::onExit(const Controller& controller) {
  std::cout << "Exited" << std::endl;
}

void SampleListener::onFrame(const Controller& controller) {
  // Get the most recent frame and report some basic information

  //std::cout << "New Frame" << std::endl;
  const Frame frame = controller.frame();






  mutex.lock();

  HandList hands = frame.hands();
  for (HandList::const_iterator hl = hands.begin(); hl != hands.end(); ++hl) {
    // Get the first hand
    const Hand hand = *hl;
    std::string handType = hand.isLeft() ? "Left hand" : "Right hand";
    std::cout << std::string(2, ' ') << handType << ", id: " << hand.id()
              << ", palm position: " << hand.palmPosition() << std::endl;

    const Vector position = hand.palmPosition();

    float freq = tones[(int)((std::max(std::min(position.x, 300.0f), -300.0f) + 300.0f) / 600.0f * 6)];
    freq *= std::pow(2, (int)((std::max(std::min(position.y, 500.0f), 100.0f) -100.0f) / 400.0f * 4));

    float modulation = (int) std::abs((std::max(std::min(position.z, 300.0f), -300.0f) / 300.0f * 5));

    //freq = floorf(freq);

    //std::cout << modulation << std::endl;

    if (hand.isLeft()) {
      freqLeft = freq;
      modulationLeft = modulation;
      //volumeLeft = vol;
    } else {
      freqRight = freq;
      modulationRight = modulation;
      //volumeRight = vol;
    }
  }

 mutex.unlock();

}

void SampleListener::onFocusGained(const Controller& controller) {
  std::cout << "Focus Gained" << std::endl;
}

void SampleListener::onFocusLost(const Controller& controller) {
  std::cout << "Focus Lost" << std::endl;
}

void SampleListener::onDeviceChange(const Controller& controller) {
  std::cout << "Device Changed" << std::endl;
  const DeviceList devices = controller.devices();

  for (int i = 0; i < devices.count(); ++i) {
    std::cout << "id: " << devices[i].toString() << std::endl;
    std::cout << "  isStreaming: " << (devices[i].isStreaming() ? "true" : "false") << std::endl;
  }
}

void SampleListener::onServiceConnect(const Controller& controller) {
  std::cout << "Service Connected" << std::endl;
}

void SampleListener::onServiceDisconnect(const Controller& controller) {
  std::cout << "Service Disconnected" << std::endl;
}

//-------------------------------------------------------------------------
// Audio side
//-------------------------------------------------------------------------



static unsigned int sinIndex = 0;

void checkFortPortAudioError(PaError err, const char *prefix = "PortAudio error") {
  if (err != paNoError) {
    std::stringstream str;
    str << prefix << " : " << Pa_GetErrorText(err);
    throw std::runtime_error(str.str());
  }
}

static inline float genSinWave(float freq) {
  return std::sin(sinIndex * 2.0 * M_PI * (freq / 44100.0f));
}

static float genTone(float freq) {
  return genSinWave(freq) + genSinWave(freq*2) + genSinWave(freq*4); + genSinWave(freq*8) * 0.2;
}

static int paCallback(
  const void *inputBuffer, 
  void *outputBuffer,
    unsigned long framesPerBuffer,
    const PaStreamCallbackTimeInfo* timeInfo,
    PaStreamCallbackFlags statusFlags,
    void *userData
) {
  mutex.lock();
  //const float *in = (const float *) inputBuffer;
  float *out = (float *) outputBuffer;

  for (int i=0; i<framesPerBuffer; i++) {
    *out++ = genTone(freqRight) * volumeRight * std::cos(sinIndex * 2.0 * M_PI * (modulationRight / 44100.0f));
    *out++ = genTone(freqLeft) * volumeLeft * std::cos(sinIndex * 2.0 * M_PI * (modulationLeft / 44100.0f));

    sinIndex++;
  }

  mutex.unlock();

  return 0;
}

//---------------------------------------------------------------------------

int main(int argc, char** argv) {
  // Create a sample listener and controller
  SampleListener listener;
  Controller controller;

  // Have the sample listener receive events from the controller
  controller.addListener(listener);

  if (argc > 1 && strcmp(argv[1], "--bg") == 0)
    controller.setPolicy(Leap::Controller::POLICY_BACKGROUND_FRAMES);

  PaStream *stream;

  //----------------------
  //Audio side
  //----------------------

  int numInputChannel = 0;
  int numOutputChannel = 2;
  PaSampleFormat sampleFormat = paFloat32;
  double sampleRate = 44100;

  PaError err = Pa_Initialize();

  checkFortPortAudioError(err, "initialize()");

  std::cout << "Default input = " << Pa_GetDefaultInputDevice() << std::endl;
  std::cout << "Default output = " << Pa_GetDefaultOutputDevice() << std::endl;

  PaStreamParameters outputParameters;
  PaStreamParameters inputParameters;

  /* -- setup input and output -- */
  inputParameters.device = Pa_GetDefaultInputDevice(); 
  inputParameters.channelCount = numInputChannel;
  inputParameters.sampleFormat = sampleFormat;
  inputParameters.suggestedLatency = Pa_GetDeviceInfo( inputParameters.device )->defaultLowInputLatency ;
  inputParameters.hostApiSpecificStreamInfo = NULL;
  
  outputParameters.device = Pa_GetDefaultOutputDevice();
  outputParameters.channelCount = numOutputChannel;
  outputParameters.sampleFormat = sampleFormat;
  outputParameters.suggestedLatency = Pa_GetDeviceInfo( outputParameters.device )->defaultLowOutputLatency;
  outputParameters.hostApiSpecificStreamInfo = NULL;

  err = Pa_OpenStream(
    &stream,
    NULL,
    &outputParameters,
    sampleRate,
    paFramesPerBufferUnspecified,
    paClipOff,
    paCallback,
    NULL
  );

  checkFortPortAudioError(err, "openStream()");

  err = Pa_StartStream(stream);

  checkFortPortAudioError(err, "startStream()");

  // Keep this process running until Enter is pressed
  std::cout << "Press Enter to quit..." << std::endl;
  std::cin.get();

  // Remove the sample listener when done
  controller.removeListener(listener);

  return 0;
}
