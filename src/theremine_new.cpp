#include "synthetizer_controller.h"

#include <SFML/Window.hpp>

#include <string>
#include <fstream>
#include <streambuf>
#include <iostream>
#include <chrono>

#include <GL/glew.h>

#include "3rdparty/json.hpp"

#include "g3d2/gl_buffer.h"
#include "g3d2/gl_program.h"
#include "g3d2/matrix.h"
#include "g3d2/vector.h"
#include "g3d2/texture.h"
#include "g3d2/particle_emitter.h"

using Time = std::chrono::time_point<std::chrono::system_clock>;
using Duration = std::chrono::duration<float>;
using Clock = std::chrono::system_clock;

using json = nlohmann::json;

struct Params {
    bool fullScreen = false;
    int soundDeviceId = StereoToneSynthetizer::getDefaultOutputDevice();
    float tapeCenterX = 0.0f;
    float tapeCenterY = 0.0f;
    float tapeWidth = 0.01f;
    float tapeSize = 0.5f;
    float tapeRotation = 0.0f;
    float slotsOpacity = 0.8f;
    int slotCount = 30;
};

std::string readFile(std::string fileName) {
	std::ifstream t(fileName);
    if (!t.good()) {
        std::stringstream stream;
        stream << "Failed to open file " << fileName;
        throw std::runtime_error(stream.str());
    }
	return std::string((std::istreambuf_iterator<char>(t)),
                 std::istreambuf_iterator<char>());
}

void readParameters(std::string fileName, Params &params) {
    std::string content;

    try {
        content = readFile(fileName);
    } catch (std::exception &) {
        return;
    }

    auto &readParams = json::parse(content);

    for (auto &elem : readParams.items()) {
        if (elem.key().compare("soundDeviceId") == 0) {
            params.soundDeviceId = elem.value().get<int>();
        }

        if (elem.key().compare("fullScreen") == 0) {
            params.fullScreen = elem.value().get<bool>();
        }

        if (elem.key().compare("tapeCenterX") == 0) {
            params.tapeCenterX = elem.value().get<float>();
        }

        if (elem.key().compare("tapeCenterY") == 0) {
            params.tapeCenterY = elem.value().get<float>();
        }

        if (elem.key().compare("tapeWidth") == 0) {
            params.tapeWidth = elem.value().get<float>();
        }

        if (elem.key().compare("tapeSize") == 0) {
            params.tapeSize = elem.value().get<float>();
        }

        if (elem.key().compare("tapeRotation") == 0) {
            params.tapeRotation = elem.value().get<float>();
        }

        if (elem.key().compare("slotsOpacity") == 0) {
            params.slotsOpacity = elem.value().get<float>();
        }

        if (elem.key().compare("slotCount") == 0) {
            params.slotCount = elem.value().get<int>();
        }
    }
}

void saveParameters(std::string fileName, Params &params) {
    json toWrite;

    toWrite["soundDeviceId"] = params.soundDeviceId;
    toWrite["fullScreen"] = params.fullScreen;
    toWrite["tapeCenterX"] = params.tapeCenterX;
    toWrite["tapeCenterY"] = params.tapeCenterY;
    toWrite["tapeWidth"] = params.tapeWidth;
    toWrite["tapeSize"] = params.tapeSize;
    toWrite["tapeRotation"] = params.tapeRotation;
    toWrite["slotsOpacity"] = params.slotsOpacity;
    toWrite["slotCount"] = params.slotCount;

    std::ofstream out(fileName);
    out << toWrite.dump(4);
    out.close();
}

void mainLoop(Params& params) {

	SynthetizerController controller(params.soundDeviceId);

	sf::Window window(sf::VideoMode::getFullscreenModes()[0], "OpenGL", params.fullScreen ? sf::Style::Fullscreen : sf::Style::Default, sf::ContextSettings(32));
    window.setVerticalSyncEnabled(true);

    glewInit();

    sf::ContextSettings settings = window.getSettings();

    std::cout << "depth bits:" << settings.depthBits << std::endl;
    std::cout << "stencil bits:" << settings.stencilBits << std::endl;
    std::cout << "antialiasing level:" << settings.antialiasingLevel << std::endl;
    std::cout << "version:" << settings.majorVersion << "." << settings.minorVersion << std::endl;

    G3D::GLProgram program(readFile("vertex.glsl"), readFile("fragment.glsl"));

    const unsigned short indices[] = { 0, 1, 2, 2, 3, 0 };

    const float positions[] = {
        -1.0f, -1.0f,
         1.0f, -1.0f,
         1.0f,  1.0f,
        -1.0f,  1.0f
    };

    G3D::GLElementBuffer indexBuffer(indices, 6);
    G3D::GLVertexBuffer vertexBuffer(positions, 8);

    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);

    bool running = true;

    Time startTime = Clock::now();

    while (running) {

    	sf::Event event;

        while (window.pollEvent(event))
        {
        	
        	switch (event.type) {
        		case sf::Event::Closed:
        			running = false;
        		break;

        		case sf::Event::Resized:
        			 glViewport(0, 0, event.size.width, event.size.height);
        		break;

        		case sf::Event::KeyPressed:
        			if (event.key.code == sf::Keyboard::Escape) {
        				running = false;
        			}

        			if (event.key.code == sf::Keyboard::O) {
        				params.slotCount++;
        			}

        			if (event.key.code == sf::Keyboard::L) {
        				params.slotCount--;
        			}

                    if (event.key.code == sf::Keyboard::S) {
                        saveParameters("config.json", params);
                    }
        	}
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) {
        	params.tapeCenterX += 0.01f;
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) {
        	params.tapeCenterX -= 0.01f;
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) {
            params.tapeCenterY += 0.01f;
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) {
            params.tapeCenterY -= 0.01f;
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::I)) {
        	params.tapeSize += 0.01f;
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::K)) {
        	params.tapeSize -= 0.01f;
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Y)) {
            params.tapeWidth += 0.001f;
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::H)) {
            params.tapeWidth -= 0.001f;
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::U)) {
            params.slotsOpacity += 0.01f;
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::J)) {
            params.slotsOpacity -= 0.01f;
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::T)) {
            params.tapeRotation += 0.01f;
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::G)) {
            params.tapeRotation -= 0.01f;
        }

        controller.update();

        float currentTime = Duration(Clock::now() - startTime).count();

        // effacement les tampons de couleur/profondeur
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glEnable(GL_BLEND);
        glBlendFunc(GL_ONE, GL_ONE);
		glDepthMask(false);

        program.use();

        glUniform1fv(program.getUniform("fTime").location, 1, &currentTime);

        glUniform2fv(program.getUniform("fCenter").location, 1, &(params.tapeCenterX));
        glUniform1i(program.getUniform("iSlotCount").location, params.slotCount);
        glUniform1f(program.getUniform("fSlotsOpacity").location, params.slotsOpacity);
        glUniform1f(program.getUniform("fTapeSize").location, params.tapeSize);
        glUniform1f(program.getUniform("fTapeWidth").location, params.tapeWidth);
        glUniform1f(program.getUniform("fTapeRotation").location, params.tapeRotation);
        glUniform3fv(program.getUniform("vLeftHand").location, 1, controller.getLeftHandPosition());
        glUniform3fv(program.getUniform("vRightHand").location, 1, controller.getRightHandPosition());

        glEnableVertexAttribArray(program.getAttribut("pPosition").location);
        vertexBuffer.bind();
        glVertexAttribPointer(program.getAttribut("pPosition").location, 2, GL_FLOAT, false, 0, 0);

        indexBuffer.bind();

        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, 0);

        window.display();
    }
}

int main(int argc, char **argv) {
    Params params;

    readParameters("config.json", params);

	for (int i=1; i<argc; i++) {
		std::string arg(argv[1]);
		if (arg.compare("-l") == 0) {
			std::vector<std::string> devicesDesc = StereoToneSynthetizer::enumerateSoundDevices();

			std::cout << devicesDesc.size() << " devices found : " << std::endl;

			for (auto &deviceDesc : devicesDesc) {
				std::cout << deviceDesc << std::endl;
			}

			return 0;
		}

        if (arg.compare("-d") == 0 && argc > i + 2) {
            try {
                params.soundDeviceId = std::stoi(argv[i+1]);
                i++;
            } catch (std::invalid_argument &) {
                std::cout << "Invalide device index." << std::endl;
            }
        }

        if (arg.compare("-f") == 0) {
            params.fullScreen = true;
        }
	}

	mainLoop(params);

	return 0;
}