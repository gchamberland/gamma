#ifndef CUTOFF_FILTER_H
#define CUTOFF_FILTER_H

#include <cmath>

#define TWO_PI 6.28318530718f
#define ONE_ON_SQRT_TWO 0.7071067811865475244008f

class CutoffFilter {
	public:
		enum Type {
			LOW_PASS,
			HIGH_PASS
		};

	private:
		float a0, a1, a2, b1, b2;
		float x1 = 0, x2 = 0, y1 = 0, y2 = 0;

	public:
		CutoffFilter(float cutoffFrequency, float sampleRate = 44100.0f,
			float quality = ONE_ON_SQRT_TWO, Type type = HIGH_PASS
		) {
			float theta = TWO_PI * (cutoffFrequency / sampleRate);
			float d = 0.5f * (1.0f / quality) * std::sin(theta);
			float beta = 0.5f * ( (1.0f - d) / (1.0f + d) );
			float gamma = (0.5f + beta) * std::cos(theta);

			if (type == LOW_PASS) {
				a0 = 0.5f * (0.5f + beta - gamma);
				a1 = 0.5f + beta - gamma;
			} else {
				a0 = 0.5f * (0.5f + beta + gamma);
				a1 = -(0.5f + beta + gamma);
			}

			a2 = a0;
			b1 = -2.0f * gamma;
			b2 = 2.0f * beta;
		}

		float process(float sample) {
			float out = a0 * sample + a1 * x1 + a2 * x2 - b1 * y1 - b2 * y2;
			x2 = x1;
			x1 = sample;
			y2 = y1;
			y1 = out;
			return out;
		}
};

#endif