#ifndef DOUBLE_TONE_SYNTHETIZER_H
#define DOUBLE_TONE_SYNTHETIZER_H

#include "abstract_synthetizer.h"
#include "delay.h"
#include "cutoff_filter.h"

#include <algorithm>

#include <g3d2/random.h>

#define TWO_PI 6.28318530718f

template <typename T> T CLAMP(const T& value, const T& low, const T& high) 
{
  return value < low ? low : (value > high ? high : value); 
}

class StereoToneSynthetizer : public AbstractSynthetizer {
	public:
		enum Range {
			SEMI_TONIC,
			TONIC
		};

	private:
		int freqIndexLeft = 0, freqIndexRight = 0;
		float modulationLeft = 0, modulationRight = 0;
		float octaveRight = 1.0f, octaveLeft = 1.0f;
		float volume = 0.1f;
		Delay dr1 = Delay(0.17f, 44100, 0);
		Delay dr2 = Delay(0.3f, 44100, 0);
		Delay dl1 = Delay(0.17f, 44100, 0);
		Delay dl2 = Delay(0.3f, 44100, 0);
		CutoffFilter cr = CutoffFilter(5);
		CutoffFilter cl = CutoffFilter(5);

		unsigned int sinIndex = 0;

		Range range = TONIC;

		static const std::vector<float> &tonicRange() {
			static std::vector<float> tonicRange = {
				  0.0f,
				  87.31f,
				  103.83f,
				  116.54f,
				  174.61f
			};
			return tonicRange;
		}

		static const std::vector<float> &tonicRange2() {
			static std::vector<float> tonicRange = {
				  0.0f,
				  130.81f,
				  155.56f,
				  174.61f,
				  261.63f
			};
			return tonicRange;
		}

		static const std::vector<float> &semiTonicRange() {
			static std::vector<float> semiTonicRange = {
				  32.70f,
				  34.65f,
				  73.42f,
				  38.89f,
				  41.20f,
				  43.65f,
				  46.25f,
				  49.00f,
				  51.91f,
				  110.00f,
				  58.27f,
				  61.74f
			};
			return semiTonicRange;
		}

		inline float genSinWave(float freq) {
		  return std::sin(sinIndex * TWO_PI * (freq / getSampleRate()));
		}

		inline float genSquareWave(float freq) {
			return genSinWave(freq) >= 0.0f ? 1.0f : 0.0f;
		}

		inline float genTriangleWave(float freq) {
			float p = 1.0f / (freq / getSampleRate());
			float localTime = std::fmodf((float) sinIndex, p);
			float val = localTime / p;

			return val < 0.25f ? val * 4.0f :
					val < 0.75f ? 2.0f - val * 4.0f :
					 val * 4.0f - 4.0f;
		}

		inline float genSawtoothWave(float freq) {
			float p = 1.0f / (freq / getSampleRate());
			float localTime = fmodf((float) sinIndex, p);

			return localTime / p * 2.0f - 1.0f;
		}

		float genTone(int freqIndex, float octave) {
		  return (genSawtoothWave(tonicRange()[freqIndex] * octave) + genSawtoothWave(tonicRange2()[freqIndex] * octave)
		  			+ genSawtoothWave(tonicRange()[freqIndex] * octave * 3.0f)) + genSawtoothWave(tonicRange2()[freqIndex] * octave * 5.0f) * 0.25f;
		}

	protected:
		virtual int synthetizeSound(
			void *outputBuffer,
			unsigned long framesPerBuffer,
			const PaStreamCallbackTimeInfo* timeInfo,
			PaStreamCallbackFlags statusFlags
		) {
			float *out = reinterpret_cast<float *>(outputBuffer);

			for (unsigned long i=0; i<framesPerBuffer; i++) {
				*out++ = dl1.process(dl2.process(cl.process(
					genTone(freqIndexLeft, octaveLeft) * std::cos(sinIndex * TWO_PI * (modulationLeft / getSampleRate()))
				))) * volume;

				*out++ =dr1.process(dr2.process(cr.process(
					genTone(freqIndexRight, octaveRight) * std::cos(sinIndex * TWO_PI * (modulationRight / getSampleRate()))
				))) * volume;

				sinIndex++;
			}

			if (sinIndex > 44100) {
				dl1.setDecay(0.3f);
				dl2.setDecay(0.2f);
				dr1.setDecay(0.3f);
				dr2.setDecay(0.2f);
			}

			if (sinIndex == 441000) {
				sinIndex = 0;
			}

			return 0;
		}

	public:
		void setRange(Range range) {
			this->range = range;
		}

		const  std::vector<float> &getRange() {
			return this->range == SEMI_TONIC ?
				semiTonicRange() : tonicRange();
		}

		void playToneLeft(unsigned int index) {
			auto range = this->range == SEMI_TONIC ?
				semiTonicRange() : tonicRange();

			freqIndexLeft = index % range.size();
		}

		void playToneRight(unsigned int index) {
			auto range = this->range == SEMI_TONIC ?
				semiTonicRange() : tonicRange();

			freqIndexRight = index % range.size();
		}

		void setModulationLeft(float modulation) {
			modulationLeft = modulation;
		}

		void setModulationRight(float modulation) {
			modulationRight = modulation;
		}

		void setVolume(float volume) {
			this->volume = volume;
		}

		void setOctaveRight(int octave) {
			octaveRight = (float) CLAMP(octave, 0, 8);
		}

		void setOctaveLeft(int octave) {
			octaveLeft = (float) CLAMP(octave, 0, 8);
		}
};

#endif