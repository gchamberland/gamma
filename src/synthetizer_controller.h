#ifndef SYNTHETIZER_CONTROLLER_H
#define SYNTHETIZER_CONTROLLER_H

#include "stereo_tone_synthetizer.h"

#include <Leap.h>

#include <chrono>
#include <iostream>

class SynthetizerController : public Leap::Listener {
	using Duration = std::chrono::duration<float>;
	using Clock = std::chrono::system_clock;
	using Time = std::chrono::time_point<std::chrono::system_clock>;

	private:
		Leap::Controller controller;
		StereoToneSynthetizer synthetizer;
		float leftHandPos[3];
		float rightHandPos[3];
		Time lastInput;

		void init() {
    		leftHandPos[0] = 0;
   			leftHandPos[1] = 0.1f;
   			leftHandPos[2] = 0.1f;

   			rightHandPos[0] = 0;
   			rightHandPos[1] = 0;
   			rightHandPos[2] = 0.15f;

   			synthetizer.setOctaveLeft(0);
			synthetizer.playToneLeft(0);
			synthetizer.setModulationLeft(0);

			synthetizer.setOctaveRight(0);
			synthetizer.playToneRight(0);
			synthetizer.setModulationRight(0);
    	}


	public:
    	void onInit(const Leap::Controller&) {
    		std::cout << "Leap motion contoller initialized" << std::endl;
    	}

   		void onConnect(const Leap::Controller&) {
   			std::cout << "Connected with leap motion device" << std::endl;
   			//synthetizer.start();
   		}

    	void onDisconnect(const Leap::Controller&) {
    		std::cout << "Leap motion device disconnected" << std::endl;
    		//synthetizer.stop();
    	}

    	const float *getLeftHandPosition() const {
    		return leftHandPos;
    	}

    	const float *getRightHandPosition() const {
    		return rightHandPos;
    	}

    	void update() {
    		const Leap::Frame frame = controller.frame();

    		Leap::HandList hands = frame.hands();

    		if (Duration(Clock::now() - lastInput).count() > 10.0f) {
    			init();
    		}

			for (Leap::HandList::const_iterator hl = hands.begin(); hl != hands.end(); ++hl) {

				const Leap::Hand hand = *hl;

				const Leap::Vector position = hand.palmPosition();

				float x = (CLAMP(position.x, -300.0f, 300.0f) + 300.0f) / 600.0f;
				float y = (CLAMP(position.y, 0.0f, 500.0f)) / 500.0f;
				float z = std::abs(CLAMP(position.z, -300.0f, 300.0f)) / 300.0f;

				int tone = (int) (y * synthetizer.getRange().size());
				int octave = (int) (1.0f +  x * 3.0f);
				int modulation = (int) (z * 10.0f);

				if (hand.isLeft()) {
					synthetizer.setOctaveLeft(octave);
					synthetizer.playToneLeft(tone);
					synthetizer.setModulationLeft((float) modulation);
					leftHandPos[0] = x;
					leftHandPos[1] = y;
					leftHandPos[2] = z;
				} else {
					synthetizer.setOctaveRight(octave);
					synthetizer.playToneRight(tone);
					synthetizer.setModulationRight((float) modulation);
					rightHandPos[0] = x;
					rightHandPos[1] = y;
					rightHandPos[2] = z;
				}

				lastInput = Clock::now();
			  }
    	}

    	SynthetizerController(int deviceId) {
   			controller.setPolicy(Leap::Controller::POLICY_BACKGROUND_FRAMES);

   			synthetizer.start(deviceId);

   			init();

    		//synthetizer.setRange(StereoToneSynthetizer::Range::SEMI_TONIC);
    	}
};

#endif