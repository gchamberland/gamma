#ifndef ABSTRACT_SYNTHETIZER_H
#define ABSTRACT_SYNTHETIZER_H

#include <portaudio.h>

#include <string>
#include <vector>
#include <sstream>

#include <iostream>

class AbstractSynthetizer {
	private:
		float sampleRate = -1;
		int numOutputChannel = -1;
		int outputDevice = -1;

		PaStream *stream;

		static bool &portAudioInitialized() {
			static bool portAudioInitialized = false;
			return portAudioInitialized;
		}

		static void checkFortPortAudioError(PaError err, const char *prefix = "PortAudio error") {
			if (err != paNoError) {
				std::stringstream str;
				str << prefix << " : " << Pa_GetErrorText(err);
				#if _WIN32
					std::cout << str.str() << std::endl;
				#endif
				throw std::runtime_error(str.str());
			}
		}

		static void initPortAudio() {
			if (portAudioInitialized()) {
				return;
			}

			checkFortPortAudioError(Pa_Initialize());

			portAudioInitialized() = true;
		}

		friend int portAudioCallback(
			const void *inputBuffer, 
			void *outputBuffer,
			unsigned long framesPerBuffer,
			const PaStreamCallbackTimeInfo* timeInfo,
			PaStreamCallbackFlags statusFlags,
			void *userData
		);

	protected:
		virtual int synthetizeSound(
			void *outputBuffer,
			unsigned long framesPerBuffer,
			const PaStreamCallbackTimeInfo* timeInfo,
			PaStreamCallbackFlags statusFlags
		) = 0;

		virtual void afterStart() {

		}

	public:
		static std::vector<std::string> enumerateSoundDevices() {
			initPortAudio();

			int deviceCount = Pa_GetDeviceCount();

			if (deviceCount < 0) {
				checkFortPortAudioError(deviceCount);
			}

			int defaultOutputDevice = Pa_GetDefaultOutputDevice();

			if (defaultOutputDevice < 0) {
				checkFortPortAudioError(defaultOutputDevice);
			}

			std::vector<std::string> results;

			const PaDeviceInfo *deviceInfo;
			for(int i=0; i<deviceCount; i++ )
			{
			    deviceInfo = Pa_GetDeviceInfo( i );

			    std::stringstream stream;
			    stream << "Device " << i << " : ";
			    stream << deviceInfo->name << ", ";
			    stream << "Max output channels : " << deviceInfo->maxOutputChannels;
			    
			    if (i == defaultOutputDevice) {
			    	stream << " **DEFAULT**";
			    }

			    results.push_back(stream.str());
			}

			return results;
		}

		float getSampleRate() {
			return this->sampleRate;
		}

		int getNumOutputChannel() {
			return this->numOutputChannel;
		}

		bool isStarted() {
			initPortAudio();

			PaError error = Pa_IsStreamActive(this->stream);

			if (error < 0) {
				checkFortPortAudioError(error);
			}
			return error == 1;
		}

		static int getDefaultOutputDevice() {
			initPortAudio();
			return Pa_GetDefaultOutputDevice();
		}

		int getOutputDevice() {
			return this->outputDevice;
		}

		void start(int deviceId = getDefaultOutputDevice(), int numOutputChannel = 2, double sampleRate = 44100) {
			initPortAudio();

			PaStreamParameters outputParameters;

			outputParameters.device = deviceId;
			outputParameters.channelCount = numOutputChannel;
			outputParameters.sampleFormat = paFloat32;
			outputParameters.suggestedLatency = Pa_GetDeviceInfo( outputParameters.device )->defaultLowOutputLatency;
			outputParameters.hostApiSpecificStreamInfo = NULL;

			PaError error = Pa_OpenStream(
				&this->stream,
				NULL,
				&outputParameters,
				sampleRate,
				paFramesPerBufferUnspecified,
				paNoFlag,
				portAudioCallback,
				this
			);

			checkFortPortAudioError(error);

			error = Pa_StartStream(stream);

  			checkFortPortAudioError(error);

  			this->outputDevice = deviceId;
  			this->numOutputChannel = numOutputChannel;
			this->sampleRate = (float) sampleRate;

			afterStart();
		}

		void stop() {
			initPortAudio();

			PaError error = Pa_StopStream(this->stream);

			checkFortPortAudioError(error);

			error = Pa_CloseStream(this->stream);

			checkFortPortAudioError(error);
		}

		void restart(int deviceId = Pa_GetDefaultOutputDevice(), int numOutputChannel = 2, double sampleRate = 44100) {
			if (isStarted()) {
				stop();
			}

			start(deviceId, numOutputChannel, sampleRate);
		}
};

int portAudioCallback(
	const void *inputBuffer, 
	void *outputBuffer,
	unsigned long framesPerBuffer,
	const PaStreamCallbackTimeInfo* timeInfo,
	PaStreamCallbackFlags statusFlags,
	void *userData
) {
	AbstractSynthetizer *target = reinterpret_cast<AbstractSynthetizer *>(userData);
	return target->synthetizeSound(
		outputBuffer, framesPerBuffer, 
		timeInfo, statusFlags
	);
}

#endif